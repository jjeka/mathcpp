/* 
 * File:       Main.cpp
 * Author:     Jeka
 * Project:    MathCPP library
 * Repository: https://bitbucket.org/jjeka/mathcpp
 *
 */

#include <iostream>
#include "MathCpp.h"

using MathCpp::operator "" _solve;

int main()
{
    constexpr auto n = "(67+987^(7-3*2))*(34-123)+17^2+(-1)"_solve;
    std::cout << "Answer is " << n;
}