/* 
 * File:       CMathVariable.h
 * Author:     Jeka
 * Project:    MathCPP library
 * Repository: https://bitbucket.org/jjeka/mathcpp
 *
 */

#ifndef CMATHVARIABLE
#define	CMATHVARIABLE

#include <ostream>
#include <cmath>

namespace MathCpp
{

class CMathVariable
{
private:

    int64_t numerator_;
    uint64_t denominator_;

    constexpr CMathVariable (int64_t numerator, uint64_t denominator);
    
    constexpr int64_t sign_ (int64_t a) const;
    constexpr uint64_t gcd_ (uint64_t a, uint64_t b) const;
    constexpr CMathVariable reduce_() const;

public:
    
    constexpr explicit CMathVariable (int number);
    
    constexpr CMathVariable operator + (const CMathVariable& n) const;
    constexpr CMathVariable operator - (const CMathVariable& n) const;
    constexpr CMathVariable operator * (const CMathVariable& n) const;
    constexpr CMathVariable operator / (const CMathVariable& n) const;

    constexpr int64_t numerator() const;
    constexpr uint64_t denominator() const;
    constexpr bool is_plus_inf() const;
    constexpr bool is_menus_inf() const;
    constexpr bool is_nan() const;
    constexpr bool is_inf() const;
    constexpr bool is_usual() const;
    constexpr bool is_integer() const;
    
    constexpr int to_int() const;
    constexpr int force_to_int() const;
    constexpr double to_double() const;
    
    friend constexpr CMathVariable operator - (const CMathVariable& n);
    friend constexpr CMathVariable operator + (const CMathVariable& n);
    friend std::ostream& operator << (std::ostream& os, const CMathVariable& var);
    
};

constexpr CMathVariable operator - (const CMathVariable& n);
constexpr CMathVariable operator + (const CMathVariable& n);
std::ostream& operator << (std::ostream& os, const CMathVariable& var);

constexpr CMathVariable::CMathVariable (int number) : 
    numerator_ (number), 
    denominator_ (1)
{
}

constexpr CMathVariable::CMathVariable (int64_t numerator, uint64_t denominator) : 
    numerator_ (numerator), 
    denominator_ (denominator)
{
}

constexpr int64_t CMathVariable::sign_ (int64_t a) const
{
    return (a > 0) - (a < 0);
}

constexpr uint64_t CMathVariable::gcd_ (uint64_t a, uint64_t b) const
{
    return (b == 0) ? a : gcd_ (b, a % b);
}

constexpr CMathVariable CMathVariable::reduce_() const
{
    return (numerator_ == 0) ? CMathVariable (0, sign_ (denominator_)) : 
        ((denominator_ == 0) ? CMathVariable (sign_ (numerator_), 0) : 
            CMathVariable (numerator_ / gcd_ (static_cast<uint64_t> (std::abs (numerator_)), denominator_), 
                           denominator_ / gcd_ (static_cast<uint64_t> (std::abs (numerator_)), denominator_)));
}

constexpr int64_t CMathVariable::numerator() const
{
	return numerator_;
}

constexpr uint64_t CMathVariable::denominator() const
{
	return denominator_;
}

constexpr bool CMathVariable::is_plus_inf() const
{
	return denominator_ == 0 && numerator_ > 0;
}

constexpr bool CMathVariable::is_menus_inf() const
{
    return denominator_ == 0 && numerator_ < 0;
}

constexpr bool CMathVariable::is_nan() const
{
    return denominator_ == 0 && numerator_ == 0;
}

constexpr bool CMathVariable::is_inf() const
{
    return denominator_ == 0 && numerator_ != 0;
}

constexpr bool CMathVariable::is_usual() const
{
    return denominator_ != 0;
}

constexpr bool CMathVariable::is_integer() const
{
    return denominator_ == 1;
}

constexpr int CMathVariable::to_int() const
{
    return static_cast<int> (numerator_ / denominator_);
}

constexpr int CMathVariable::force_to_int() const
{
    return (!(denominator_ == 1 && static_cast<int> (numerator_) == numerator_) ? 
           (throw std::logic_error ("CMathVariable can't be represented by int"), 0) : 0), to_int();
}

constexpr double CMathVariable::to_double() const
{
    return static_cast<double> (numerator_) / denominator_;
}

constexpr CMathVariable CMathVariable::operator + (const CMathVariable& n) const
{
    return CMathVariable (
        static_cast<int64_t> (n.denominator_ / gcd_ (denominator_, n.denominator_)) * numerator_ + 
	static_cast<int64_t> (denominator_ / gcd_ (denominator_, n.denominator_)) * n.numerator_, 
        
        denominator_ / gcd_ (denominator_, n.denominator_) * n.denominator_).reduce_();
}

constexpr CMathVariable CMathVariable::operator - (const CMathVariable& n) const
{
    return CMathVariable (
        static_cast<int64_t> (n.denominator_ / gcd_ (denominator_, n.denominator_)) * numerator_ - 
	static_cast<int64_t> (denominator_ / gcd_ (denominator_, n.denominator_)) * n.numerator_, 
        
        denominator_ / gcd_ (denominator_, n.denominator_) * n.denominator_).reduce_();
}

constexpr CMathVariable CMathVariable::operator * (const CMathVariable& n) const
{
    return CMathVariable (
            numerator_ * n.numerator_, 
            denominator_ * n.denominator_).reduce_();
}

constexpr CMathVariable CMathVariable::operator / (const CMathVariable& n) const
{
    return CMathVariable (
            numerator_ * static_cast<int64_t> (n.denominator_) * (n.numerator_ ? sign_ (n.numerator_) : 1), 
            denominator_ * static_cast<uint64_t> (std::abs (n.numerator_))).reduce_();
}

constexpr CMathVariable operator + (const CMathVariable& n)
{
    return n;
}

constexpr CMathVariable operator - (const CMathVariable& n)
{
    return CMathVariable (-n.numerator_, n.denominator_);
}

std::ostream& operator << (std::ostream& stream, const CMathVariable& var)
{
    if (var.is_plus_inf())
        stream << "+inf";
    else if (var.is_menus_inf())
        stream << "-inf";
    else if (var.is_nan())
        stream << "nan";
    else if (var.denominator() == 1)
        stream << var.numerator();
    else
        stream << var.numerator() << " / " << var.denominator();
    
    return stream;
}

}; // namespace MathCpp

#endif // CMATHVARIABLE