/* 
 * File:       MathCpp.h
 * Author:     Jeka
 * Project:    MathCPP library
 * Repository: https://bitbucket.org/jjeka/mathcpp
 *
 */

#ifndef MATHCPP
#define MATHCPP

#include <stdexcept>
#include "CMathVariable.h"

namespace MathCpp
{

//typedef int CMathVariable;
    
struct SMathValue
{
    constexpr SMathValue (const CMathVariable _value, const int _end) :
        value (_value), 
        end (_end)
    {}
    
    constexpr SMathValue add_end (int dend) const
    {
        return SMathValue (value, end + dend);
    }
    
    const CMathVariable value;
    const int end;
};

constexpr const SMathValue INVALID_VALUE = SMathValue (CMathVariable (0), 0);

#define math_assert(condition,description) ((condition) ? INVALID_VALUE : (throw std::logic_error (description), INVALID_VALUE))

struct SMathData
{
    constexpr SMathData (const char* _str, const int _size, const int _start) :
        str (_str), 
        size (_size), 
        start (_start)
    {}
    
    constexpr SMathData create (const int _start) const
    {
        return SMathData (str, size, _start);
    }
    
    constexpr char char_start() const
    {
        return char_at (start);
    }
    constexpr char char_at (const int pos) const
    {
        return (pos >= 0 && pos < size) ? str[pos] : 
            ((pos == size) ? 0 : 
                (math_assert (false, "Internal error: out of bounds"), 0));
    }
    
    const char* str;
    const int size;
    const int start;
};

constexpr CMathVariable operator "" _solve (const char* str, const size_t size);
constexpr CMathVariable solve (const char* str, const size_t size);

template <int N>
int math_error_not_constexpr();
constexpr CMathVariable math_pow (const CMathVariable x, const CMathVariable y);
    constexpr CMathVariable _math_pow (const CMathVariable x, const int y, const CMathVariable value);
constexpr CMathVariable get_value (const int size, const SMathValue value);

constexpr SMathValue get_number (const SMathData data);
    constexpr SMathValue _get_number (const SMathData data, const int i, const bool positive);
    constexpr int _get_number_end (const SMathData data);

constexpr SMathValue get_branum (const SMathData data);
    
constexpr SMathValue get_pow (const SMathData data);
    constexpr SMathValue _get_pow (const SMathData data, const CMathVariable value);

constexpr SMathValue get_muldiv (const SMathData data);
    constexpr SMathValue _get_muldiv (const SMathData data, const CMathVariable value);

constexpr SMathValue get_addsub (const SMathData data);
    constexpr SMathValue _get_addsub (const SMathData data, const CMathVariable value);

//========================================================================================

constexpr CMathVariable math_pow (const CMathVariable x, const CMathVariable y)
{
    return math_assert (y.numerator() >= 0 && y.is_integer(), "Power can't be negative or factorial"), 
        _math_pow (x, y.to_int(), CMathVariable (1));
}

constexpr CMathVariable _math_pow (const CMathVariable x, const int y, const CMathVariable value)
{
    return (y == 0) ? value : (x * _math_pow (x, y - 1, value));
}

constexpr CMathVariable get_value (const int size, const SMathValue value)
{
    return math_assert (value.end + 1 == size, "Digit or operator required"), value.value;
}

//========================================================================================

constexpr CMathVariable operator "" _solve (const char* str, const size_t size)
{
    return solve (str, size);
}

constexpr CMathVariable solve (const char* str, const size_t size)
{
    return get_value (static_cast<int> (size), get_addsub (SMathData (str, static_cast<int> (size), 0)));
}

//========================================================================================

constexpr SMathValue get_number (const SMathData data)
{
    return (data.char_start() == '-') ? 
        (math_assert (data.char_at (data.start + 1) >= '0' && data.char_at (data.start + 1) <= '9', "Not a digit"), 
            _get_number (data.create (data.start + 1), _get_number_end (data.create (data.start + 1)), false)) : 
        (math_assert (data.char_start() >= '0' && data.char_start() <= '9', "Digit required"), 
            _get_number (data, _get_number_end (data), true));
}

constexpr SMathValue _get_number (const SMathData data, const int i, const bool positive)
{
    return
        (i >= data.start) ? 
            SMathValue (_get_number (data, i - 1, positive).value * CMathVariable (10) + 
                CMathVariable ((positive ? 1 : -1) * (data.char_at (i) - '0')), i)
        : SMathValue (CMathVariable (0), data.start - 1);
}

constexpr int _get_number_end (const SMathData data)
{
    return (data.char_start() >= '0' && data.char_start() <= '9') ? 
            _get_number_end (data.create (data.start + 1)) : (data.start - 1);
}

//========================================================================================

constexpr SMathValue get_branum (const SMathData data)
{
    return (data.char_start() == '(') ? 
        (math_assert (data.char_at (get_addsub (data.create (data.start + 1)).end + 1) == ')', "')' required"), 
         get_addsub (data.create (data.start + 1)).add_end (1))
        : get_number (data);
}

//========================================================================================

constexpr SMathValue get_pow (const SMathData data)
{
    return _get_pow (data.create (get_branum (data).end + 1), get_branum (data).value);
}

constexpr SMathValue _get_pow (const SMathData data, const CMathVariable value)
{
    return (data.char_start() == '^') ? 
        _get_pow (data.create 
        // start
        (get_branum (data.create (data.start + 1)).end + 1),     
        // value
        math_pow (value, get_branum (data.create (data.start + 1)).value))
        : SMathValue (value, data.start - 1);
}

//========================================================================================

constexpr SMathValue get_muldiv (const SMathData data)
{
    return _get_muldiv (data.create (get_pow (data).end + 1), get_pow (data).value);
}

constexpr SMathValue _get_muldiv (const SMathData data, const CMathVariable value)
{
    return (data.char_start() == '*') ? 
        _get_muldiv (data.create
        // start
        (get_pow (data.create (data.start + 1)).end + 1),     
        // value
        value * get_pow (data.create (data.start + 1)).value)
        
        : ((data.char_start() == '/') ? 
        (get_pow (data.create (data.start + 1)).value.numerator() == 0)
            ? math_assert (false, "Division by zero")
        : _get_muldiv (data.create
        // start
        (get_pow (data.create (data.start + 1)).end + 1),     
        // value
        value / get_pow (data.create (data.start + 1)).value)       
        
        : SMathValue (value, data.start - 1));
}

//========================================================================================

constexpr SMathValue get_addsub (const SMathData data)
{
    return _get_addsub (data.create (get_muldiv (data).end + 1), get_muldiv (data).value);
}

constexpr SMathValue _get_addsub (const SMathData data, const CMathVariable value)
{
    return (data.char_start() == '+') ? 
        _get_addsub (data.create
        // start
        (get_muldiv (data.create (data.start + 1)).end + 1),     
        // value
        value + get_muldiv (data.create (data.start + 1)).value)
        
        : ((data.char_start() == '-') ? 
        _get_addsub (data.create
        // start
        (get_muldiv (data.create (data.start + 1)).end + 1),     
        // value
        value - get_muldiv (data.create (data.start + 1)).value)
        
        : SMathValue (value, data.start - 1));
}

//========================================================================================

#undef math_assert

}; // namespace MathCpp

#endif // MATHCPP